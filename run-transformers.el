;;; run-transformers.el --- a construct for making extensible pipelines

;; Author: contrapunctus <xmpp:contrapunctus@jabber.fr>
;; Maintainer: contrapunctus <xmpp:contrapunctus@jabber.fr>
;; Keywords: extensions
;; Homepage: https://codeberg.org/contrapunctus/run-transformers
;; Version: 0.0.1

;;; Commentary:
;; This is a common pattern which I have used in at least two of my
;; extensions, and has also been used by at least Ivy and Company (and
;; possibly others). I hope that's reason enough to make a package for
;; it, no matter how small. Once And Only Once :D
;;
;; The idea is to have a list of functions, which can be added to by
;; other programs - effectively extending the main program without it
;; needing to know about these extensions.
;;
;; There were a variety of possible names, but I went with
;; `run-transformers' - "run-" so as to be consistent with
;; `run-hooks'/`run-hooks-with-args', and "transformers" because it's
;; what Ivy and Company call the pattern.

(defun run-transformers (transformers arg)
  "Run TRANSFORMERS with ARG.
TRANSFORMERS should be a list of functions (F₁ ... Fₙ), each of
which should accept a single argument.

Call F₁ with ARG, with each following function being called with
the return value of the previous function.

Return the value returned by Fₙ."
  (if transformers
      (dolist (fn transformers arg)
        (setq arg (funcall fn arg)))
    arg))

(provide 'run-transformers)

;;; run-transformers.el ends here
