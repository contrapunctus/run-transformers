;;; -*- lexical-binding: t; -*-
(require 'buttercup)
(require 'run-transformers)

(describe
 "run-transformers"
 :var ((transformer-list '((lambda (a) (1+ a)) (lambda (a) (+ 2 a)))))
 (it "returns arguments if transformer is nil"
     (expect (run-transformers nil 1) :to-be 1))
 (it "works with a single argument"
     (expect (run-transformers transformer-list 1) :to-be 4)))
